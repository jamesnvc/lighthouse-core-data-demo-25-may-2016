//
//  Foodstuff+CoreDataProperties.h
//  FoodJournalDemo
//
//  Created by James Cash on 25-05-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Foodstuff.h"

NS_ASSUME_NONNULL_BEGIN

@class EatenFood;

@interface Foodstuff (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSDecimalNumber *calories;
@property (nullable, nonatomic, retain) NSString *servingSize;
@property (nullable, nonatomic, retain) NSSet<EatenFood *> *timesEaten;

@end

@interface Foodstuff (CoreDataGeneratedAccessors)

- (void)addTimesEatenObject:(EatenFood *)value;
- (void)removeTimesEatenObject:(EatenFood *)value;
- (void)addTimesEaten:(NSSet<EatenFood *> *)values;
- (void)removeTimesEaten:(NSSet<EatenFood *> *)values;

@end

NS_ASSUME_NONNULL_END
