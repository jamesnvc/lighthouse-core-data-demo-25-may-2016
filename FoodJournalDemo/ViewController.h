//
//  ViewController.h
//  FoodJournalDemo
//
//  Created by James Cash on 25-05-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource>


@end

