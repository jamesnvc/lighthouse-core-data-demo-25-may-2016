//
//  EatenFood.m
//  FoodJournalDemo
//
//  Created by James Cash on 25-05-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "EatenFood.h"
#import "Foodstuff.h"

@implementation EatenFood

- (NSString*)dayEaten
{
    NSDateFormatter *dfmt = [[NSDateFormatter alloc] init];
    dfmt.dateStyle = NSDateFormatterShortStyle;
    dfmt.timeStyle = NSDateFormatterNoStyle;
    return [dfmt stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.eatenAt]];
}

@end
